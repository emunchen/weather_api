const mongoose = require("mongoose");
const app = require("./app");
const config = require("./config");
const port = config.serverPort || 3800;
mongoose.Promise = global.Promise;
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
mongoose
  .connect(config.mongoUri, options)
  .then(() => {
    console.log("DB connected");

    app.listen(port, () => {
      console.log(`Listen on port: ${port}`);
    });
  })
  .catch((err) => console.log(err));
