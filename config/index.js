require("dotenv").config();
const httpStatus = require("./httpStatus");

module.exports = {
  env: process.env.NODE_ENV,
  mongoUri: process.env.MONGODB_URI,
  masterDb: process.env.MASTER_DB,
  serverPort: process.env.SERVER_PORT,
  serverDb: process.env.SERVER_DB,
  JWTSecret: process.env.JWT_SECRET,
  JWTHeader: process.env.JWT_HEADER,
  JWTExpiresIn: process.env.JWT_EXPIRES_IN,
  httpStatus: { ...httpStatus },
};
