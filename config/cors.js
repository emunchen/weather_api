const config = require("./index");

module.exports = {
  allowedHeaders: ["sessionId", "Content-Type", config.JWTHeader],
  exposedHeaders: ["sessionId"],
  origin: "*",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: false,
};
