const City = require("../models/city");
const { httpStatus } = require("../config");

exports.getCity = (req, res) => {
  const cityId = req.params.id;
  City.findById(cityId, (err, city) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (!city)
      return res
        .status(httpStatus.notFound)
        .send({ message: "City Not Found" });
    return res.status(httpStatus.OK).send({
      city,
    });
  });
};

exports.getCityBySlug = (req, res) => {
  const citySlug = req.params.slug;
  City.find({ slug: citySlug }, (err, city) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (!city)
      return res
        .status(httpStatus.notFound)
        .send({ message: "City Not Found" });
    return res.status(httpStatus.OK).send({
      city,
    });
  });
};

exports.getAllCities = (req, res) => {
  City.find({}, (err, cities) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (!cities)
      return res
        .status(httpStatus.notFound)
        .send({ message: "Cities Not Found" });
    return res.status(httpStatus.OK).send({
      cities,
    });
  });
};
