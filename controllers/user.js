const User = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
const { httpStatus } = config;

exports.userFromToken = (req, res, next) => {
  const { user } = req.decoded || false;
  if (!user) {
    return res
      .status(httpStatus.notAcceptable)
      .send({ message: "Failed to authenticate token." });
  }
  User.find({ _id: user._id }, (err, user) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    const payload = { user: user._id };
    const token = jwt.sign(payload, config.JWTSecret, {
      expiresIn: config.JWTExpiresIn,
    });
    return res.status(httpStatus.OK).send({ token });
  });
};

exports.getUser = (req, res, next) => {
  const userId = req.params.id;
  User.findById(userId, (err, user) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (!user)
      return res
        .status(httpStatus.notFound)
        .send({ message: "User Not Found" });
    user.password = undefined;
    return res.status(httpStatus.OK).send({
      user,
    });
  });
};

exports.getUsers = (req, res, next) => {
  User.find({}, (err, users) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (!users)
      return res
        .status(httpStatus.notFound)
        .send({ message: "Users Not Found" });
    users.map((user) => (user.password = undefined));
    return res.status(httpStatus.OK).send({
      users,
    });
  });
};

exports.createUser = (req, res, next) => {
  let newUser = new User({
    email: req.body.email,
    password: req.body.password,
    access: ["observations", "cities"],
  });

  User.findOne({ email: newUser.email }, (err, user) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (user) {
      return res
        .status(httpStatus.badRequest)
        .send({ message: `User ${user.email} already exists.` });
    }
    newUser.save((err) => {
      if (err) {
        return res.status(httpStatus.serverError).send(err);
      }
      newUser.password = undefined;
      return res.status(httpStatus.created).send({
        user: newUser,
      });
    });
  });
};

exports.login = (req, res, next) => {
  const { email, password } = req.body;
  User.findOne({ email }, (err, user) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });

    if (!user) {
      return res
        .status(httpStatus.notFound)
        .send({ message: `User not found` });
    }

    bcrypt.compare(password, user.password, (err, result) => {
      if (err) return res.status(httpStatus.serverError).send({ message: err });
      if (result === true) {
        const payload = { user: user._id };
        const token = jwt.sign(payload, config.JWTSecret, {
          expiresIn: config.JWTExpiresIn,
        });
        user.password = undefined;
        return res.status(httpStatus.OK).send({
          user,
          token,
        });
      } else {
        return res
          .status(httpStatus.notAcceptable)
          .send({ message: `Password doesn't match` });
      }
    });
  });
};
