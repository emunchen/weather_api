const Observation = require("../models/observation");
const User = require("../models/user");
const mongoose = require("mongoose");
const { httpStatus } = require("../config");

exports.getObservation = (req, res) => {
  const observationId = req.params.id;
  Observation.findById(observationId, (err, observation) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (!observation)
      return res
        .status(httpStatus.notFound)
        .send({ message: "Observation Not Found" });
    return res.status(httpStatus.OK).send({
      observation,
    });
  });
};

exports.getObservations = (req, res) => {
  Observation.find({})
    .populate("user")
    .populate("city")
    .exec((err, observations) => {
      if (err) return res.status(httpStatus.serverError).send({ message: err });

      return res.status(httpStatus.OK).send({
        observations,
      });
    });
};

exports.getObservationsByCity = (req, res) => {
  const cityId = req.params.id;
  Observation.find({})
    .populate({ path: "city", match: { _id: cityId } })
    .populate("user")
    .exec((err, observations) => {
      observations = observations.filter((observation) => {
        return observation.city;
      });
      if (err) return res.status(httpStatus.serverError).send({ message: err });

      return res.status(httpStatus.OK).send({
        observations,
      });
    });
};

exports.createObservation = (req, res) => {
  let email = req.body.email || "";
  let city_id = req.body.city_id || "";
  if (!email || !city_id) {
    res
      .status(httpStatus.badRequest)
      .send({ message: "You must provide an email and city_id" });
  }
  User.findOne({ email: email }, (err, user) => {
    if (err) return res.status(httpStatus.serverError).send({ message: err });
    if (!user) {
      let newUser = new User({
        email: email,
      });
      newUser.save((err) => {
        let observation = new Observation({
          text: req.body.text,
          user: mongoose.Types.ObjectId(newUser._id),
          city: mongoose.Types.ObjectId(city_id),
        });
        observation.save((err) => {
          if (err) {
            return next(err);
          }
          return res.status(httpStatus.OK).send({
            observation,
          });
        });
      });
    } else {
      let observation = new Observation({
        text: req.body.text,
        user: mongoose.Types.ObjectId(user._id),
        city: mongoose.Types.ObjectId(city_id),
      });
      observation.save((err) => {
        if (err) {
          return next(err);
        }
        return res.status(httpStatus.OK).send({
          observation,
        });
      });
    }
  });
};
