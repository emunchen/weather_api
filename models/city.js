const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const Schema = mongoose.Schema;
const CitySchema = new Schema({
  name: String,
  wind: Number,
  latitude: {
    orientation: String,
    value: String,
  },
  longitude: {
    orientation: String,
    value: String,
  },
});
CitySchema.plugin(mongoosePaginate);
const City = mongoose.model("City", CitySchema);
module.exports = City;
