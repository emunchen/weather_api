const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const Schema = mongoose.Schema;
const ObservationSchema = new Schema({
  text: String,
  created_at: {
    type: Date,
    default: Date.now,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  city: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "City",
  },
});
ObservationSchema.plugin(mongoosePaginate);
const Observation = mongoose.model("Observation", ObservationSchema);
module.exports = Observation;
