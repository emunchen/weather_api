const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const corsConfig = require("./config/cors");
const app = express();
const city_routes = require("./routes/city");
const observation_routes = require("./routes/observation");
const user_routes = require("./routes/user");
const validateToken = require("./middlewares/validate-token");

app.use(cors(corsConfig));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/api", user_routes);
app.use("/api", city_routes);
app.use("/api", validateToken, observation_routes);

module.exports = app;
