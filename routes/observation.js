const express = require("express");
const ObservationController = require("../controllers/observation");
const api = express.Router();

api.get("/observations/:id", ObservationController.getObservation);
api.get("/observations/:id/cities", ObservationController.getObservationsByCity);
api.get("/observations", ObservationController.getObservations);
api.post("/observations", ObservationController.createObservation);
module.exports = api;
