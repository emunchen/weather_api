const express = require("express");
const UserController = require("../controllers/user");
const api = express.Router();
const validateToken = require("./../middlewares/validate-token");

api.get("/users/me", validateToken, UserController.userFromToken);
api.get("/users/:id", validateToken, UserController.getUser);
api.get("/users", validateToken, UserController.getUsers);
api.post("/users", UserController.createUser);
api.post("/users/login", UserController.login);
module.exports = api;
