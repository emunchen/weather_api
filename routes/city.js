const express = require("express");
const CityController = require("../controllers/city");
const api = express.Router();

api.get("/cities/:id", CityController.getCity);
api.get("/cities/:slug/_slug", CityController.getCityBySlug);
api.get("/cities", CityController.getAllCities);
module.exports = api;
