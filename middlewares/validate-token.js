const jwt = require("jsonwebtoken");
const config = require("../config");
const { httpStatus } = config;

const getTokenFromHeaders = (req) => {
  let token =
    req.body.token || req.query.token || req.headers[config.JWTHeader];
  token = token ? token.replace("Bearer ", "") : undefined;
  return token;
};

const validateToken = (req, res, next) => {
  const token = getTokenFromHeaders(req);
  if (token) {
    jwt.verify(token, config.JWTSecret, (err, decoded) => {
      if (err) {
        return res
          .status(httpStatus.notAcceptable)
          .send({ message: "Failed to authenticate token." });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(httpStatus.forbidden).send({
      message: "No token provided.",
    });
  }
};

module.exports = validateToken;
