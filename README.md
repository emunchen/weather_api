# Weather API

These instructions are to install Weather API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Clone this repo

```
git clone git@bitbucket.org:emunchen/weather_api.git
```

Install Node using NVM

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

### Installing

A step by step series of examples that tell you have to get a development env running

```
npm install

```

## Running

Run the main script.

```
npm start
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [Git](https://git-scm.com/) for versioning.

## Authors

- **Emanuel Pecora** - _Initial work_ - [Emunchen](https://github.com/emunchen)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
